#!/bin/bash

set -ue
DIRECTORY="D:/yenmoc/Workspace/yenmoc/git/upm-cli"
source "${DIRECTORY}/scripts/lib/function/exists.sh"

source "${DIRECTORY}/scripts/lib/validation/npmrc.sh"
source "${DIRECTORY}/scripts/lib/validation/upm-config.sh"

config_file="${HOME}/.upm-config.json"

source "${DIRECTORY}/scripts/lib/variable/registries.sh"
source "${DIRECTORY}/scripts/lib/validation/registries.sh"

source "${DIRECTORY}/scripts/lib/variable/registry-index.sh"
source "${DIRECTORY}/scripts/lib/validation/registry-index.sh"
source "${DIRECTORY}/scripts/lib/variable/registry-name.sh"

source "${DIRECTORY}/scripts/lib/validation/registry-existance.sh"

source "${DIRECTORY}/scripts/lib/variable/package-name.sh"
source "${DIRECTORY}/scripts/lib/validation/package-name.sh"

source "${DIRECTORY}/scripts/lib/variable/display-name.sh"
source "${DIRECTORY}/scripts/lib/validation/display-name.sh"

source "${DIRECTORY}/scripts/lib/variable/description.sh"
# Description has no validation

registry_json=$(cat ${config_file} | jq ".\"registries\".\"${REGISTRY_NAME}\"")
registry_hostname=$(echo ${registry_json} | jq -r '."hostname"')
registry_protocol=$(echo ${registry_json} | jq -r '."protocol"')

project_name=${PACKAGE_NAME}
package_domain=$(echo ${registry_json} | jq -r '."domain"')
package_name=$(echo ${PACKAGE_NAME} | tr '[:upper:]' '[:lower:]')
package_name=${package_name//\./-}
display_name=${DISPLAY_NAME}
description=${DESCRIPTION}
unity_version=$(echo ${registry_json} | jq -r '."unity_version"')
author_name=$(echo ${registry_json} | jq -r '."author"."name"')
author_url=$(echo ${registry_json} | jq -r '."author"."url"')
author_email=$(echo ${registry_json} | jq -r '."author"."email"')
company_name="DefaultCompany"
if [ "null" != "$(echo ${registry_json} | jq -r '."company"."name"')" ]; then
  company_name=$(echo ${registry_json} | jq -r '."company"."name"')
fi
repository_type=$(echo ${registry_json} | jq -r '."repository"."type"')
repository_user=$(echo ${registry_json} | jq -r '."repository"."user"')
if [ -z "${repository_user}" ]; then
  repository_user=$(echo ${registry_json} | jq -r '."repository"."organization"')
fi
repository_name=${PACKAGE_NAME//\./-}
license="UNLICENSED"
license_url=""
if [ "string" = "$(echo ${registry_json} | jq -r '.license|type')" ]; then
  license=$(echo ${registry_json} | jq -r '.license')
elif [ "object" = "$(echo ${registry_json} | jq -r '.license|type')" ] && [ "null" != "$(echo ${registry_json} | jq -r '.license.type')" ]; then
  license=$(echo ${registry_json} | jq -r '.license.type')
  if [ "null" != "$(echo ${registry_json} | jq -r '.license.url')" ]; then
    license_url=$(echo ${registry_json} | jq -r '.license.url')
  fi
fi

if [ -d "${repository_name}" ]; then
  cat << __VALIDATE_DIRECTORY__
ERROR: Directory '${repository_name}' has already exists
__VALIDATE_DIRECTORY__
  exit 1
fi

package_json=$(cat << __PACKAGE_JSON__
{
  "name": "${package_domain}.${package_name}",
  "displayName": "${display_name}",
  "version": "0.0.1",
  "unity": "${unity_version}",
  "description": "${description}",
  "author": {
    "name": "${author_name}",
    "url": "${author_url}",
    "email": "${author_email}"
  },
  "license": "${license}",
  "keywords": [
    "unity"
  ],
  "category": "",
  "dependencies": {
  },
  "repository": {
    "type": "${repository_type}",
    "url": "https://gitlab.com/${repository_user}/${repository_name}"
  }
}
__PACKAGE_JSON__
)

mkdir -p "${repository_name}/Assets"
mkdir -p "${repository_name}/ProjectSettings"
cd "${repository_name}"
echo ${package_json} | jq -M '.' > Assets/package.json
#echo "registry=${registry_protocol}://${registry_hostname}" > .npmrc
echo "registry=${registry_protocol}://localhost:4873" > Assets/.npmrc

cat > Assets/README.md << __README__
# ${display_name}

## Requirements
[![Unity 2019.3+](https://img.shields.io/badge/unity-2019.3+-brightgreen.svg?style=flat&logo=unity&cacheSeconds=2592000)](https://unity3d.com/get-unity/download/archive)
[![.NET 4.x Scripting Runtime](https://img.shields.io/badge/.NET-4.x-blueviolet.svg?style=flat&cacheSeconds=2592000)](https://docs.unity3d.com/2019.1/Documentation/Manual/ScriptingRuntimeUpgrade.html)

## Installation

\`\`\`bash
"${package_domain}.${package_name}":"https://gitlab.com/${repository_user}/${repository_name}"
or
npm publish --registry http://localhost:4873
\`\`\`


__README__

cat > Assets/CHANGELOG.md << __CHANGELOG__
# Changelog
All notable changes to this project will be documented in this file.

## [0.0.1] - DD-MM-YYYY
### Added
	-

### Changed
	-

### Removed
	-
	
__CHANGELOG__

cat > Assets/.npmignore << __NPMIGNORE__
.npmignore
Modules/
Modules.meta
Plugins/
Plugins.meta
!Plugins/*.dll
!Plugins/Android/AndroidManifest.xml
Tests/
Tests.meta
Benchmarks/
Benchmarks.meta

__NPMIGNORE__

cat > ProjectSettings/ProjectSettings.asset << __PROJECT_SETTINGS__
%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!129 &1
PlayerSettings:
  companyName: ${company_name}
  productName: ${project_name}
  applicationIdentifier:
    Android: ${package_domain}.${package_name}
__PROJECT_SETTINGS__
curl -so .gitignore https://gist.githubusercontent.com/phongsoyenmoc/96bf5917a49cb0170ff6021f15488b55/raw/046caa17439950b8adb40c676f6147480fc70e66/.gitignore
git init
git remote add origin git@gitlab.com:${repository_user}/${repository_name}.git
if [ -n "${license_url}" ]; then
  curl -so Assets/LICENSE.txt ${license_url}
fi

cat << __MESSAGE__
Finish prepare to developing Unity Package !
🚀 What's next ?
  cd ${repository_name}
  # edit Assets/README.md
  # edit Assets/package.json if needed
  git add .
  git commit -m ":tada: Initialized"
  git push origin master

Enjoy development 🎉
__MESSAGE__

$SHELL
