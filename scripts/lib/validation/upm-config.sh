#!/bin/bash

if [ ! -e "${HOME}/.upm-config.json" ]; then
  cat << __VALIDATE_UPM_CONFIG__
ERROR: Could not find ${HOME}/.upm-config.json
  Please create like below configuration file

{
  "registries": {
    "com.yenmoc.modules": {
      "name": "Yenmoc Registry",
      "protocol": "https",
      "hostname": "com.yenmoc.modules",
      "scopes": [
        "com.yenmoc"
      ],
      "unity_version": "2019.2",
      "author": {
        "name": "yenmoc",
        "url": "https://gitlab.com/phongsoyenmoc/",
        "email": "phongsoyenmoc.diep@gmail.com"
      },
      "company": {
        "name": "yenmoc"
      },
      "license": {
        "type": "MIT",
        "url": "https://gist.githubusercontent.com/phongsoyenmoc/a02eb8113ff0eca6d7ae4dd4ba87edbd/raw/605302cb51f0fb327a8725175711e68b173a70cc/LICENSE.txt"
      },
      "repository": {
        "type": "git",
        "user": "yenmoc",
        "organization": "yenmoc"
      },
      "domain": "com.yenmoc"
    }
  }
}
__VALIDATE_UPM_CONFIG__
  exit 1
fi
